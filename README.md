# KSN Syntax for Sublime Text

Syntax Highlighting for Sublime Text for .ksn files used by [Koui](https://gitlab.com/koui/Koui).

## Installation

Currently, this package is not yet available from [packagecontrol](packagecontrol.io).

1. Open Sublime Text and go to `Preferences > Browse Packages`

2. Clone or download this repository and paste the project directory into the directory that was opened in step 1.

3. Open a `.ksn` file in Sublime Text and go to `View > Syntax > Open all with current extension as... > Koui Style Notation (KSN)`



![](./screenshot.png)